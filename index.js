/* STAND IN LINE ACTIVITY*/

function nextInLine(arr, item) {
  // Only change code below this line
  arr.push(item);
  const removed = arr.shift();
  return removed;
  // Only change code above this line
}

// Setup
const testArr = [1, 2, 3, 4, 5];
const arr2 =  [2, 3, 4, 5, 6];
const arr3 =  [5, 6, 7, 8, 9];
const arr4 =  [5, 6, 7, 8, 9];

// Display code
console.log("Before: " + JSON.stringify(testArr));
console.log(nextInLine(testArr, 6));
console.log("After: " + JSON.stringify(testArr));

console.log("Before: " + JSON.stringify(arr2));
console.log(nextInLine(arr2, 7));
console.log("After: " + JSON.stringify(arr2));

console.log("Before: " + JSON.stringify(arr3));
console.log(nextInLine(arr3, 10));
console.log("After: " + JSON.stringify(arr3));

console.log("Before: " + JSON.stringify(arr4));
console.log(nextInLine(arr4, 10));
console.log("After: " + JSON.stringify(arr4));


/* IF STATEMENTS 
	if statements are used to make decisions in code. The keyword if tells JavaScript to execute the code in the curly braces under certain conditions, defined in the parentheses. These conditions are known as Boolean conditions and they may only be true or false.

	When the condition evaluates to true, the program executes the statement inside the curly braces. When the Boolean condition evaluates to false, the statement inside the curly braces will not execute.
*/

function trueOrFalse(wasThatTrue) {
  // Only change code below this line
if (wasThatTrue == true){
  return "Yes, that was true";
}
  return "No, that was false";

}
console.log(trueOrFalse(true));
console.log(trueOrFalse(false));


/* COMPARISON WITH THE EQUALITY OPERATOR

	There are many comparison operators in JavaScript. All of these operators return a boolean true or false value.

	The most basic operator is the equality operator ==. The equality operator compares two values and returns true if they're equivalent or false if they are not. Note that equality is different from assignment (=), which assigns the value on the right of the operator to a variable on the left.

	Ex:
	1   ==  1  // true
	1   ==  2  // false
	1   == '1' // true
	"3" ==  3  // true
*/

function testEqual(val) {
  if (val == 12) { // Change this line
    return "Equal";
  }
  return "Not Equal";
}

console.log(testEqual(12));
console.log(testEqual(10));

/* COMPARISON WITH STRICT EQUALITY OPERATOR

	Strict equality (===) is the counterpart to the equality operator (==). However, unlike the equality operator, which attempts to convert both values being compared to a common type, the strict equality operator does not perform a type conversion.

	If the values being compared have different types, they are considered unequal, and the strict equality operator will return false.

	Ex:
	3 ===  3  // true
	3 === '3' // false
*/

function testStrict(val) {
  if (val === 7) { // Change this line
    return "Equal";
  }
  return "Not Equal";
}

console.log(testStrict(10));
console.log(testStrict(7));
console.log(testStrict("7"));


/* ACTIVITY: 
	compareEquality(10, "10") should return the string Not Equal

	compareEquality("20", 20) should return the string Not Equal

	You should use the === operator
*/
function compareEquality(a, b) {
  if (a === b) { // Change this line
    return "Equal";
  }
  return "Not Equal";
}

console.log(compareEquality(10, "10"));
console.log(compareEquality("20", 20)); 



/* Comparison with the Inequality Operator

	The inequality operator (!=) is the opposite of the equality operator. It means not equal and returns false where equality would return true and vice versa. Like the equality operator, the inequality operator will convert data types of values while comparing.

	Examples:
	1 !=  2    // true
	1 != "1"   // false
	1 != '1'   // false
	1 != true  // false
	0 != false // false
*/

function testNotEqual(val) {
  if (val != "99") { // Change this line
    return "Not Equal";
  }
  return "Equal";
}

console.log(testNotEqual(99));
console.log(testNotEqual("99"));
console.log(testNotEqual(12));
console.log(testNotEqual("12"));
console.log(testNotEqual("12"));



/* Comparison with the Strict Inequality Operator

	The strict inequality operator (!==) is the logical opposite of the strict equality operator. It means "Strictly Not Equal" and returns false where strict equality would return true and vice versa. The strict inequality operator will not convert data types.

	Examples
	3 !==  3  // false
	3 !== '3' // true
	4 !==  3  // true
*/

function testStrictNotEqual(val) {
  if (val !== 17) { // Change this line
    return "Not Equal";
  }
  return "Equal";
}

console.log(testStrictNotEqual(17));
console.log(testStrictNotEqual("17"));
console.log(testStrictNotEqual(12));
console.log(testStrictNotEqual("bob"));


/* COMPARISON WITH THE GREATER THAN OPERATOR (>)
	The greater than operator (>) compares the values of two numbers. If the number to the left is greater than the number to the right, it returns true. Otherwise, it returns false.

	Like the equality operator, the greater than operator will convert data types of values while comparing.

	Examples:
	5   >  3  // true
	7   > '3' // true
	2   >  3  // false
	'1' >  9  // false
*/

function testGreaterThan(val) {
  if (val > 100) {  // Change this line
    return "Over 100";
  }

  if (val > 10) {  // Change this line
    return "Over 10";
  }

  return "10 or Under";
}

console.log(testGreaterThan(0));
console.log(testGreaterThan(10));
console.log(testGreaterThan(11));
console.log(testGreaterThan(99));
console.log(testGreaterThan(100));
console.log(testGreaterThan(101));


/* Comparison with the Greater Than Or Equal To Operator
	The greater than or equal to operator (>=) compares the values of two numbers. If the number to the left is greater than or equal to the number to the right, it returns true. Otherwise, it returns false.

	Like the equality operator, the greater than or equal to operator will convert data types while comparing.

	Ex:
	6   >=  6  // true
	7   >= '3' // true
	2   >=  3  // false
	'7' >=  9  // false
*/

function testGreaterOrEqual(val) {
  if (val >= 20) {  // Change this line
    return "20 or Over";
  }

  if (val >= 10) {  // Change this line
    return "10 or Over";
  }

  return "Less than 10";
}

console.log(testGreaterOrEqual(0));
console.log(testGreaterOrEqual(9));
console.log(testGreaterOrEqual(10));
console.log(testGreaterOrEqual(11));
console.log(testGreaterOrEqual(19));
console.log(testGreaterOrEqual(100));
console.log(testGreaterOrEqual(21));

/* Comparison with the Less Than Operator
	The less than operator (<) compares the values of two numbers. If the number to the left is less than the number to the right, it returns true. Otherwise, it returns false. Like the equality operator, the less than operator converts data types while comparing.

Examples:
2   < 5 // true
'3' < 7 // true
5   < 5 // false
3   < 2 // false
'8' < 4 // false
*/
function testLessThan(val) {
  if (val < 25) {  // Change this line
    return "Under 25";
  }

  if (val < 55) {  // Change this line
    return "Under 55";
  }

  return "55 or Over";
}
console.log(testLessThan(0));
console.log(testLessThan(24));
console.log(testLessThan(25));
console.log(testLessThan(54));
console.log(testLessThan(55));
console.log(testLessThan(99));



/*Comparison with the Less Than Or Equal To Operator

	The less than or equal to operator (<=) compares the values of two numbers. If the number to the left is less than or equal to the number to the right, it returns true. If the number on the left is greater than the number on the right, it returns false. Like the equality operator, the less than or equal to operator converts data types.

Examples
4   <= 5 // true
'7' <= 7 // true
5   <= 5 // true
3   <= 2 // false
'8' <= 4 // false

*/
function testLessOrEqual(val) {
  if (val <= 12) {  // Change this line
    return "Smaller Than or Equal to 12";
  }

  if (val <= 24) {  // Change this line
    return "Smaller Than or Equal to 24";
  }

  return "More Than 24";
}

console.log(testLessOrEqual(0));
console.log(testLessOrEqual(11));
console.log(testLessOrEqual(12));
console.log(testLessOrEqual(23));
console.log(testLessOrEqual(24));
console.log(testLessOrEqual(25));
console.log(testLessOrEqual(55));


/*Comparisons with the Logical And Operator
	--> Sometimes you will need to test more than one thing at a time. The logical and operator (&&) returns true if and only if the operands to the left and right of it are true.

	The same effect could be achieved by nesting an if statement inside another if:

	if (num > 5) {
	  if (num < 10) {
	    return "Yes";
	  }
	}
	return "No";
	
	--> will only return Yes if num is greater than 5 and less than 10. The same logic can be written as:

	if (num > 5 && num < 10) {
	  return "Yes";
	}
	return "No";
*/

function testLogicalAnd(val) {
  // Only change code below this line

  if (val >=25 && val <= 50){
      return "Yes";
  }

  // Only change code above this line
  return "No";
}

console.log(testLogicalAnd(0));
console.log(testLogicalAnd(24));
console.log(testLogicalAnd(25));
console.log(testLogicalAnd(30));
console.log(testLogicalAnd(50));
console.log(testLogicalAnd(51));
console.log(testLogicalAnd(75));
console.log(testLogicalAnd(80));


/* Comparisons with the Logical Or Operator
	The logical or operator (||) returns true if either of the operands is true. Otherwise, it returns false.

	The logical or operator is composed of two pipe symbols: (||). This can typically be found between your Backspace and Enter keys.

	The pattern below should look familiar from prior waypoints:

			if (num > 10) {
			  return "No";
			}
			if (num < 5) {
			  return "No";
			}
			return "Yes";

	--> will return Yes only if num is between 5 and 10 (5 and 10 included). The same logic can be written as:

	if (num > 10 || num < 5) {
	  return "No";
	}
	return "Yes";
*/

function testLogicalOr(val) {
  // Only change code below this line

  if (val < 10 || val > 20) {
    return "Outside";
  }

  // Only change code above this line
  return "Inside";
}

console.log(testLogicalOr(0));
console.log(testLogicalOr(9));
console.log(testLogicalOr(10));
console.log(testLogicalOr(15));
console.log(testLogicalOr(19));
console.log(testLogicalOr(20));
console.log(testLogicalOr(21));
console.log(testLogicalOr(25));


/* Introducing Else Statements
	--> When a condition for an if statement is true, the block of code following it is executed. What about when that condition is false? Normally nothing would happen. With an else statement, an alternate block of code can be executed.

	if (num > 10) {
	  return "Bigger than 10";
	} else {
	  return "10 or Less";
	}

*/
function testElse(val) {
  let result = "";
  // Only change code below this line

  if (val > 5) {
    result = "Bigger than 5";
  } else {
    result = "5 or Smaller";
  }

  // Only change code above this line
  return result;
}

console.log(testElse(4));
console.log(testElse(5));
console.log(testElse(6));
console.log(testElse(10));


/* Introducing Else If Statements
	-> If you have multiple conditions that need to be addressed, you can chain if statements together with else if statements.

	if (num > 15) {
	  return "Bigger than 15";
	} else if (num < 5) {
	  return "Smaller than 5";
	} else {
	  return "Between 5 and 15";
	}
	
*/
function testElseIf(val) {
  if (val > 10) {
    return "Greater than 10";
  } else if (val < 5) {
    return "Smaller than 5";
  } else {

  return "Between 5 and 10";
}
}
console.log(testElseIf(0));
console.log(testElseIf(5));
console.log(testElseIf(7));
console.log(testElseIf(10));
console.log(testElseIf(12));

/* Logical Order in If Else Statements 
	--> Order is important in if, else if statements.

The function is executed from top to bottom so you will want to be careful of what statement comes first.

Take these two functions as an example.

Here's the first:

function foo(x) {
  if (x < 1) {
    return "Less than one";
  } else if (x < 2) {
    return "Less than two";
  } else {
    return "Greater than or equal to two";
  }
}
And the second just switches the order of the statements:

function bar(x) {
  if (x < 2) {
    return "Less than two";
  } else if (x < 1) {
    return "Less than one";
  } else {
    return "Greater than or equal to two";
  }
}

--> While these two functions look nearly identical if we pass a number to both we get different outputs.

foo(0)
bar(0)
foo(0) 

-->will return the string Less than one, and bar(0) will return the string Less than two.
*/

function orderMyLogic(val) {
  if (val < 5) {
    return "Less than 5";
  } else if (val < 10) {
    return "Less than 10";
  } else {
    return "Greater than or equal to 10";
  }
}
console.log(orderMyLogic(4));
console.log(orderMyLogic(6));
console.log(orderMyLogic(10));
console.log(orderMyLogic(11));

/*Chaining If Else Statement

	if/else statements can be chained together for complex logic. Here is pseudocode of multiple chained if / else if statements:

	if (condition1) {
	  statement1
	} else if (condition2) {
	  statement2
	} else if (condition3) {
	  statement3
	. . .
	} else {
	  statementN
	}
*/

function testSize(num) {
  // Only change code below this line
  if (num < 5){
  	return "Tiny";
  } else if (num < 10) {
  	return "Small";
  } else if (num < 15) {
  	return "Medium";
  } else if (num < 20) {
  	return "Large";
  } else {
  	return "Huge";
  }
  // Only change code above this line
}

console.log(testSize(0));
console.log(testSize(4));
console.log(testSize(5));
console.log(testSize(8));
console.log(testSize(10));
console.log(testSize(14));
console.log(testSize(15));
console.log(testSize(17));
console.log(testSize(20));
console.log(testSize(25));



/* GOLF CODE
	--> In the game of golf, each hole has a par, meaning, the average number of strokes a golfer is expected to make in order to sink the ball in the hole to complete the play. Depending on how far above or below par your strokes are, there is a different nickname.

	Your function will be passed par and strokes arguments. Return the correct string according to this table which lists the strokes in order of priority; top (highest) to bottom (lowest):

	Strokes	Return
	1	"Hole-in-one!"
	<= par - 2	"Eagle"
	par - 1	"Birdie"
	par	"Par"
	par + 1	"Bogey"
	par + 2	"Double Bogey"
	>= par + 3	"Go Home!"
	par and strokes will always be numeric and positive. We have added an array of all the names for your convenience.
*/

const names = ["Hole-in-one!", "Eagle", "Birdie", "Par", "Bogey", "Double Bogey", "Go Home!"];

function golfScore(par, strokes) {
  // Only change code below this line
if (strokes == 1) {
  return names[0]; //Hole-in-one
} else if (strokes <= par - 2) {
  return names[1]; //Eagle
} else if (strokes == par - 1) {
  return names[2]; //Birdie
} else if (strokes == par) {
  return names[3]; //Par
} else if (strokes == par + 1){
  return names[4]; //Bogey
} else if (strokes == par + 2){
  return names[5]; //Double Bogey
} else if (strokes >= par + 3) {
  return names[6]; //Go Home
}
}
console.log(golfScore(4, 1));
console.log(golfScore(4, 2));
console.log(golfScore(5, 2));
console.log(golfScore(4, 3));
console.log(golfScore(4, 4));
console.log(golfScore(1, 1));
console.log(golfScore(5, 5));
console.log(golfScore(4, 5));
console.log(golfScore(4, 6));
console.log(golfScore(4, 7));
console.log(golfScore(5, 9));

/* SELECTING FROM MANY OPTIONS WITH SWITCH STATEMENTS
	--> If you have many options to choose from, use a switch statement. A switch statement tests a value and can have many case statements which define various possible values. Statements are executed from the first matched case value until a break is encountered.

	Here is an example of a switch statement:

	switch(lowercaseLetter) {
	  case "a":
	    console.log("A");
	    break;
	  case "b":
	    console.log("B");
	    break;
	}

	case values are tested with strict equality (===). The break tells JavaScript to stop executing statements. If the break is omitted, the next statement will be executed.
*/

/* ACTIVITY: 
 Write a switch statement which tests val and sets answer for the following conditions:
	1 - alpha
	2 - beta
	3 - gamma
	4 - delta
*/
function caseInSwitch(val) {
  let answer = "";
  // Only change code below this line
  switch(val){
    case 1:
    answer = "alpha";
    break;

    case 2:
    answer = "beta";
    break;

    case 3:
    answer = "gamma";
    break;

    case 4:
    answer = "delta";
    break;
  }
  return answer;
}
console.log(caseInSwitch(1));
console.log(caseInSwitch(2));
console.log(caseInSwitch(3));
console.log(caseInSwitch(4));


/* Adding a Default Option in Switch Statements
		--> In a switch statement you may not be able to specify all possible values as case statements. Instead, you can add the default statement which will be executed if no matching case statements are found. Think of it like the final else statement in an if/else chain.

A default statement should be the last case.

switch (num) {
  case value1:
    statement1;
    break;
  case value2:
    statement2;
    break;
...
  default:
    defaultStatement;
    break;
}
*/

// Activity
/* Write a switch statement to set answer for the following conditions:
a - apple
b - bird
c - cat
default - stuff
*/

function switchOfStuff(val) {
  let answer = "";
  // Only change code below this line

  switch(val){
  	case "a":
	  	return "apple";
	  	break;

  	case "b":
	  	return "bird";
	  	break;

  	case "c":
	  	return "cat";
	  	break;

  	default:
  		return "stuff";
  		break;
  }

  // Only change code above this line
  return answer;
}

console.log(switchOfStuff("a"));
console.log(switchOfStuff("b"));
console.log(switchOfStuff("c"));
console.log(switchOfStuff("d"));
console.log(switchOfStuff(4));



/* Multiple Identical Options in Switch Statements
	---> If the break statement is omitted from a switch statement's case, the following case statement(s) are executed until a break is encountered. If you have multiple inputs with the same output, you can represent them in a switch statement like this:

	let result = "";
	switch(val) {
	  case 1:
	  case 2:
	  case 3:
	    result = "1, 2, or 3";
	    break;
	  case 4:
	    result = "4 alone";
	}
	Cases for 1, 2, and 3 will all produce the same result.
*/

/* ACTIVITY:
Write a switch statement to set answer for the following ranges:
1-3 - Low
4-6 - Mid
7-9 - High*/

function sequentialSizes(val) {
  let answer = "";
  // Only change code below this line

  switch(val){
  	case 1:
  	case 2:
  	case 3:
  		answer = "Low";
  		break;
  	case 4:
  	case 5:
  	case 6:
  		answer = "Mid";
  		break;
  	case 7:
  	case 8:
  	case 9:
  		answer = "High";
  }

  // Only change code above this line
  return answer;
}

console.log(sequentialSizes(1));
console.log(sequentialSizes(2));
console.log(sequentialSizes(3));
console.log(sequentialSizes(4));
console.log(sequentialSizes(5));
console.log(sequentialSizes(6));
console.log(sequentialSizes(7));
console.log(sequentialSizes(8));
console.log(sequentialSizes(9));


/* REPLACING IF ELSE CHAINS WITH SWITCH
		If you have many options to choose from, a switch statement can be easier to write than many chained if/else if statements. The following:

		if (val === 1) {
		  answer = "a";
		} else if (val === 2) {
		  answer = "b";
		} else {
		  answer = "c";
		}
		can be replaced with:

		switch(val) {
		  case 1:
		    answer = "a";
		    break;
		  case 2:
		    answer = "b";
		    break;
		  default:
		    answer = "c";
		}
*/
function chainToSwitch(val) {
  let answer = "";
  // Only change code below this line

  switch(val) {
  	case "bob":
	  	answer = "Marley";
	  	break;

  	case 42:
  		answer = "The Answer";
  		break;

  	case 1:
  		answer = "There is no #1";
  		break;

  	case 99:
  		answer = "Missed me by this much!";
  		break;

  	case 7:
  		answer = "Ate Nine";
  		break;

  	default:
  		answer = "";

  }

  // Only change code above this line
  return answer;
}

console.log(chainToSwitch("bob"));
console.log(chainToSwitch(42));
console.log(chainToSwitch(1));
console.log(chainToSwitch(99));
console.log(chainToSwitch(7));
console.log(chainToSwitch("John"));
console.log(chainToSwitch(156));


/* RETURNING BOOLEAN VALUES FROM FUNCTIONS 
--> You may recall from Comparison with the Equality Operator that all comparison operators return a boolean true or false value.

Sometimes people use an if/else statement to do a comparison, like this:

function isEqual(a, b) {
  if (a === b) {
    return true;
  } else {
    return false;
  }
}

But there's a better way to do this. Since === returns true or false, we can return the result of the comparison:

function isEqual(a, b) {
  return a === b;
}

*/
function isLess(a, b) {
  // Only change code below this line
 return a < b;
  // Only change code above this line
}
console.log(isLess(10, 15));
console.log(isLess(15, 10));


/* RETURN EARLY PATTERN FOR FUNCTIONS
	When a return statement is reached, the execution of the current function stops and control returns to the calling location.

	Example

	function myFun() {
	  console.log("Hello");
	  return "World";
	  console.log("byebye")
	}
	myFun();

	The above will display the string Hello in the console, and return the string World. The string byebye will never display in the console, because the function exits at the return statement.
*/
function abTest(a, b) {
  // Only change code below this line

  if (a < 0 || b < 0){
  	return undefined;
  }

  // Only change code above this line

  return Math.round(Math.pow(Math.sqrt(a) + Math.sqrt(b), 2));
}
console.log(abTest(2,2));
console.log(abTest(-2,2));
console.log(abTest(2,-2));
console.log(abTest(2,8));
console.log(abTest(3,3));
console.log(abTest(0,0));


/* COUNTING CARDS GAME
	In the casino game Blackjack, a player can gain an advantage over the house by keeping track of the relative number of high and low cards remaining in the deck. This is called Card Counting.

	Having more high cards remaining in the deck favors the player. Each card is assigned a value according to the table below. When the count is positive, the player should bet high. When the count is zero or negative, the player should bet low.

	Count Change	Cards
	    +1	      2, 3, 4, 5, 6
	    0	        7, 8, 9
	    -1	      10, 'J', 'Q', 'K', 'A'
*/
var count = 0;

function cc(card) {
  if (card >= 2 && card <= 6) {
    count++;
  } else if (card >= 7 && card <= 9) {
    count += 0;
  } else if (card === 10 || card === 'J' || card === 'Q' || card === 'K' || card === 'A'){
    count--;
  }

  if (count <= 0) {
    return count + " Hold";
  } else {
    return count + " Bet";
  }
}
console.log(cc(2));
console.log(cc(3));
console.log(cc(7));
console.log(cc('K'));
console.log(cc('A'));


/* BUILD JAVASCRIPT OBJECTS
	Objects are similar to arrays, except that instead of using indexes to access and modify their data, you access the data in objects through what are called properties.

	Objects are useful for storing data in a structured way, and can represent real world objects, like a cat.

	Here's a sample cat object:

	const cat = {
	  "name": "Whiskers",
	  "legs": 4,
	  "tails": 1,
	  "enemies": ["Water", "Dogs"]
	};
	In this example, all the properties are stored as strings, such as name, legs, and tails. However, you can also use numbers as properties. You can even omit the quotes for single-word string properties, as follows:

	const anotherObject = {
	  make: "Ford",
	  5: "five",
	  "model": "focus"
	};
	However, if your object has any non-string properties, JavaScript will automatically typecast them as strings.
*/

const myDog = {
  "name": "Spikey",
  "legs": 4,
  "tails": 1,
  "friends": ["Blue", "Sasha"]
};
console.log(myDog);


/* ACCESSING PROPERTIES WITH DOT NOTATION
	There are two ways to access the properties of an object: dot notation (.) and bracket notation ([]), similar to an array.

	Dot notation is what you use when you know the name of the property you're trying to access ahead of time.

	Here is a sample of using dot notation (.) to read an object's property:

	const myObj = {
	  prop1: "val1",
	  prop2: "val2"
	};

	const prop1val = myObj.prop1;
	const prop2val = myObj.prop2;
	prop1val would have a value of the string val1, and prop2val would have a value of the string val2.
*/

const testObj = {
  "hat": "ballcap",
  "shirt": "jersey",
  "shoes": "cleats"
};
// Only change code below this line
const hatValue = testObj.hat;      
const shirtValue = testObj.shirt;
console.log(hatValue);
console.log(shirtValue); 


/* ACCESSING OBJECT PROPERTIES WITH BRACKET NOTATION
-> The second way to access the properties of an object is bracket notation ([]). If the property of the object you are trying to access has a space in its name, you will need to use bracket notation.

However, you can still use bracket notation on object properties without spaces.

Here is a sample of using bracket notation to read an object's property:

	const myObj = {
	  "Space Name": "Kirk",
	  "More Space": "Spock",
	  "NoSpace": "USS Enterprise"
	};

	myObj["Space Name"];
	myObj['More Space'];
	myObj["NoSpace"];

--> myObj["Space Name"] would be the string Kirk, myObj['More Space'] would be the string Spock, and myObj["NoSpace"] would be the string USS Enterprise.

** Note that property names with spaces in them must be in quotes (single or double).
*/   
const testObj1 = {
  "an entree": "hamburger",
  "my side": "veggies",
  "the drink": "water"
};
const entreeValue = testObj1["an entree"];   
const drinkValue = testObj1["the drink"];    
console.log(entreeValue);
console.log(drinkValue);

